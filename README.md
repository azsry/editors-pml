# Editors Promodlive

[pml220](https://github.com/promod) with additional gun ports, viewhands, and integrated bot mod based on editing community requests.

## Requirements

To compile the mod, you need to have the [CoD4 Mod Tools](https://github.com/promod/CoD4-Mod-Tools) installed in your CoD4 directory.

Once you have placed the files inside your CoD4 directory, go to the `bin` folder and run `converter.exe` at least once.

## Downloading and compiling the source

1. Clone or download the zip from the git
2. Extract zip in mods folder, or make a mod folder with the name editors-pml and drag contents in the same folder (must not be named *editorspml*)
3. To compile simply run ModBuilder.exe, type in the name of your mod folder (created in Step 2) into `Mod name`, enter a new mod name that will hold your final files in `Release mod name`, and locate your iw3mp.exe for `Game root folder`. You can leave the `Game command line arguments` field as is, unless you have specific commands you wish to execute upon opening the game.
4. Hit `Compile`. If everything worked, you should see 3 console windows open, one after the other, all without any red error messages.

Starting the game with the mod is optional. Alternatively, you can make a batch file in the mod folder you're using:

```bat
for /f "delims=" %%A in ('cd') do (
	set fn=%%~nxA
	)

cd ../..
iw3mp +set fs_game mods/%fn% +set r_mode "1280x720" +set r_fullscreen "0" +set scr_war_timelimit "0" +set scr_sd_timelimit "0"
```

If you downloaded a mod from CFGF and it comes with a source, you can easily place the rawfiles in the mod folder and add the lines from the `.csv`. The modbuilder will copy all these files over to the `../raw` directory and then proceed to compile.

## Why are we using custom viewhands?

Custom models (i.e the l96a1, or the Desert Eagle with the rolling animation) may mess with the orginal models because of the bone structure. We decided to replace the original viewhands with the viewhands from MW:Remastered. If you'd like to rollback to the original viewhands, download the character/*.gsc files from the mod tools and replace the current ones in this mod, then recompile.

###### Co-developed by Azsry and Gmzorz
