// THIS FILE IS AUTOGENERATED, DO NOT MODIFY
main()
{
	self setModel("body_mp_arab_regular_support");
	self attach("head_mp_arab_regular_asad", "", true);
	self setViewmodel("viewhands_mwr_arab");
	self.voice = "arab";
}

precache()
{
	precacheModel("body_mp_arab_regular_support");
	precacheModel("head_mp_arab_regular_asad");
	precacheModel("viewhands_mwr_arab");
}
