Write-Output "Setting path variables"
Write-Output "----------------------------------------"
$modname = "epml_testing"
$modpath = (Get-Item -Path ".\").FullName + "\"
$releasepath = $modpath + "..\$modname\"
$rootpath = $modpath + "..\..\"
$rawpath = $rootpath + "raw\"
    
$7za = $modpath + "7za.exe"
$linkerpc = $rootpath + "bin\linker_pc.exe"
$iw3mp = $rootpath + "iw3mp.exe"
    
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
    
Start-Sleep 3
    
Clear-Host
Write-Output "Cleaning current files"
Write-Output "----------------------------------------"
Remove-Item $releasepath -Recurse -Force
Remove-Item z_resource.iwd -Force
Remove-Item z_ruleset.iwd -Force
    
Start-Sleep 3
    
Clear-Host
Write-Output "Compiling Mod"
Write-Output "----------------------------------------"
    
Start-Sleep 3
    
$directories = Get-ChildItem | Where-Object { $_.PSIsContainer } | Select-Object Name
    
Foreach ( $dir in $directories ) {
    $dirname = $dir.Name.ToString()
    Write-Output "Copying $dirname to raw"
    Copy-Item -Path $dir.Name -Destination $rawpath -recurse -Force
}
Write-Output "----------------------------------------"
Write-Output "Copying mod.csv to zone_source"
Copy-Item mod.csv ($rootpath + "zone_source")
    
Write-Output "Creating .iwd's"
Write-Output "----------------------------------------"
    
& $7za a -r -mx=9 -mpass=15 -mfb=258 -mmt=on -mtc=off -tzip z_resource.iwd weapons images sound
& $7za a -r -mx=9 -mpass=15 -mfb=258 -mmt=on -mtc=off -tzip z_ruleset.iwd promod_ruleset
    
Write-Output "----------------------------------------"
    
Set-Location ($rootpath + "bin\")
    
& $linkerpc -language english -compress -cleanup mod
    
Write-Output "----------------------------------------"
    
mkdir $releasepath -Force
Set-Location $releasepath
Copy-Item -Path ($rootpath + "zone\english\mod.ff") -Destination $releasepath -Force
Copy-Item -Path ($modpath + "z_ruleset.iwd"), ($modpath + "z_resource.iwd") -Destination $releasepath -Force

$startcod4 = [System.Windows.Forms.MessageBox]::Show("Start CoD4 with mod?","Compile finished",[System.Windows.Forms.MessageBoxButtons]::YesNo,
[System.Windows.Forms.MessageBoxIcon]::Information)
if ($startcod4 -eq 'Yes') {
    Set-Location $rootpath
    & $iw3mp +set fs_game mods/epml_testing +set r_mode 7 +set r_fullscreen 0 +set scr_war_timelimit 0 +set g_gametype war
}