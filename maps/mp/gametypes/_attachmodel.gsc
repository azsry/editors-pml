AttachModel()
{
	self endon ( "disconnect" );
	self endon ( "death" );
	self endon ( "modelattached" );
	
	for(;;)
	{
		modelName = GetDvar( "sv_modelToAttach" );
		switch(GetDvarInt( "sv_attachmodel" )) {
			case 0:
				self detach( self thread FindAllAttachedModels(), "tag_stowed_back" );
				break;
				
			case 1:
				self detach( self thread FindAllAttachedModels(), "tag_stowed_back" );
				self Attach(modelName, "tag_stowed_back");
				//self notify ( "modelattached" );
				break;
		}
		wait 0.01;
	}

}

FindAllAttachedModels() {
	self endon( "death" );
	self endon( "disconnect" );
	
	for (i = 0; i <= self GetAttachSize();i++) {
		testAttach = self GetAttachTagName( i );
		if( testAttach == "tag_stowed_back" ) {
			attachedModel = self GetAttachModelName( i );
			return attachedModel;
		}
		else if( testAttach == undefined ) {
			continue;
		}
		wait 0.01;
	}
}