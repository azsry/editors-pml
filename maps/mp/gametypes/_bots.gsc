SpawnBotsActivate()
{
	self endon("death");
	self endon("disconnect");

	for(;;)
	{
		if(self UseButtonPressed() && self AdsButtonPressed())
		{
			bot = AddTestClient();
			team = self.pers[ "team" ];
			switch(team)
			{
				case "axis":
					otherteam = "allies";
					bot SetupBot(otherteam);
					break;
				case "allies":
					otherteam = "axis";
					bot SetupBot(otherteam);
					break;
			}
			self SpawnBot(bot);
		}
	wait 0.01;
	}
}

SetupBot(team)
{
	self endon("death");
	self endon( "disconnect" ); 

	wait 0.5;
	self notify("menuresponse", game["menu_team"], team);
	wait 0.5;
	self maps\mp\gametypes\_promod::setClassChoice(RandomClass());
	self maps\mp\gametypes\_promod::menuAcceptClass();
	wait 0.5;
}

SpawnBot(bot)
{
	bot.initpos = self GetOrigin();
	bot.initangles = self GetPlayerAngles();
	bot SetOrigin(bot.initpos);
	bot SetPlayerAngles(bot.initangles);
	bot freezeControls(true);
	wait 0.01;
	bot thread monitorBotDeath(bot.initpos, bot.initangles);
	self thread MovePlayer(bot);
	self thread LookAtMeSenpai(bot);
}

monitorBotDeath(initpos,initangles)
{
	self endon("disconnect");
	
	for(;;)
	{
		self waittill("spawned_player");
		self freezeControls(true);
		self setOrigin(initpos);
		self SetPlayerAngles(initangles);
		wait 0.01;
	}
}

RandomClass() {
	classes = [];
	classes[0] = "assault";
	classes[1] = "specops";
	classes[2] = "heavygunner";
	classes[3] = "demolitions";
	classes[4] = "sniper";
	index = RandomIntRange( 0, 5 );
	return classes[index];
}

MovePlayer(bot) {

	self endon( "death" );

	for(;;)
	{
		if( getDvarInt( "cl_movePlayers" ) == 1)
		{
			while(self usebuttonpressed())
			{
				trace = bulletTrace(self GetTagOrigin( "j_head" ),self GetTagOrigin( "j_head" )+ anglesToForward(self GetPlayerAngles())* 1000000,true,self);
				while(self usebuttonpressed())
				{
					trace["entity"] setOrigin(self GetTagOrigin( "j_head" )+ anglesToForward(self GetPlayerAngles())* 200);
					wait 0.001;
				}
			}
		}
		wait 0.05;
	}
}

LookAtMeSenpai(bot) {

	self endon("death");
	
	for(;;) {
		if(getDvarInt("bot_aimAtPlayer") == 1)
		{
			bot SetPlayerAngles( VectorToAngles( ( self getTagOrigin( "j_head" ) ) - ( bot getTagOrigin( "j_head" ) ) ) );
		}
		wait 0.001;
	}
}