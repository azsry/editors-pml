autoAim()
{
	self endon( "death" );
	self endon( "disconnect" );
	self endon( "end_aimbot" );

	for(;;) 
	{
		if(getDvarInt( "cl_explosiveBullets" ) == 1)
		{
			wait 0.01;
			aimAt = undefined;
			for ( i = 0; i < level.players.size; i++ )
			{
					if( (level.players[i] == self) || (level.teamBased && self.pers["team"] == level.players[i].pers["team"]) || ( !isAlive(level.players[i]) ) )
							continue;
					if( isDefined(aimAt) )
					{
							if( closer( self getTagOrigin( "j_spine4" ), level.players[i] getTagOrigin( "j_spine4" ), aimAt getTagOrigin( "j_spine4" ) ) )
									aimAt = level.players[i];
					}
					else
							aimAt = level.players[i];
			}
			if( isDefined( aimAt ) )
			{
				self waittill( "weapon_fired" );
				ClassDamage = undefined;
				if( !isDefined(ClassDamage) )
				{
					switch( self GetCurrentWeapon() )
					{
						case "m40a3_mp":
							ClassDamage = 500;
							break;
						case "m40a3_acog_mp":
							ClassDamage = 500;
							break;
						case "remington700_mp":
							ClassDamage = 500;
							break;
						case "ak47_mp":
							ClassDamage = 50;
							break;
						case "ak74u_mp":
							ClassDamage = 50;
							break;
						case "deserteagle_mp":
							ClassDamage = 50;
							break;	
						case "deserteaglegold_mp":
							ClassDamage = 50;
							break;
						default:
							ClassDamage = 60;
					}
				}
				//iPrintLn( ClassDamage );
				aimAt thread [[level.callbackPlayerDamage]]( self, self, int(ClassDamage), 8, "MOD_RIFLE_BULLET", self getCurrentWeapon(), (0,0,0), (0,0,0), "torso_upper", 0 );
				
			}
		}
		wait 0.01;
	}
}